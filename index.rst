.. Juici User Manual documentation master file, created by
   sphinx-quickstart on Mon Aug 14 13:49:45 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Juici User Manual!
=============================

Table of Contents:

.. toctree::
   :maxdepth: 2
   
   content/Document History
   content/Setup
   content/Recipe
   content/Pump Setup
   content/Sanitation



   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

